<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
<div style="height: 50px;"></div>
<div id="sideInvest">
	<h3><?php the_field('blog_side_invest_in_box_title','options'); ?></h3>
	<?php the_field('blog_side_content_area','options'); ?>
	<a href="<?php the_field('blog_side_button_link','option'); ?>"><?php the_field('blog_side_button_text','options'); ?></a>
</div>
<div style="height: 50px;"></div>
<div style="text-align: center;">
	<?php the_field('blog_side_misc_content','options'); ?>
</div>