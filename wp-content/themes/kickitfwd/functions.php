<?php
/**
 * Twenty Twenty functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

/**
 * Table of Contents:
 * Theme Support
 * Required Files
 * Register Styles
 * Register Scripts
 * Register Menus
 * Custom Logo
 * WP Body Open
 * Register Sidebars
 * Enqueue Block Editor Assets
 * Enqueue Classic Editor Styles
 * Block Editor Settings
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );




function kickitfwd_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'twentyseventeen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'twentyseventeen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'twentyseventeen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
  register_sidebar( array(
    'name'          => __( 'Woo Sidebar', 'twentyseventeen' ),
    'id'            => 'sidebar-woo',
    'description'   => __( 'WooCommerce sidebar that appears on the left.', 'twentyseventeen' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<span class="prdctfltr_regular_title">',
    'after_title'   => '</span>',
  ) );
}
add_action( 'widgets_init', 'kickitfwd_widgets_init' );


function twentytwenty_menus() {

	$locations = array(
		'primary'  => __( 'Desktop Horizontal Menu', 'twentytwenty' ),
		'expanded' => __( 'Desktop Expanded Menu', 'twentytwenty' ),
		'mobile'   => __( 'Mobile Menu', 'twentytwenty' ),
		'footer'   => __( 'Footer Menu', 'twentytwenty' ),
		'social'   => __( 'Social Menu', 'twentytwenty' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'twentytwenty_menus' );
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}


function add_theme_scripts() {
 
  wp_enqueue_style( 'style', get_template_directory_uri() . '/css/styles.css', false, '1.1', 'all');
  wp_enqueue_style( 'font-style', get_template_directory_uri() . '/css/fonts/lato/stylesheet.css', false, NULL, 'all');
  wp_enqueue_style( 'font-style2', get_template_directory_uri() . '/css/fonts/avenir/stylesheet.css', false, NULL, 'all');
  wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', false, '1.1', 'all');
  wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', false, '1.0', 'all');
  wp_enqueue_script( 'fa', 'https://kit.fontawesome.com/d2bd90a328.js', false, '1.0', 'all');
  wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-1.12.4.min.js', false, '1.0', 'all');
  wp_enqueue_script( 'jqueryui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', false, '1.0', 'all');

}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function kickitfwd_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'kickitfwd_add_woocommerce_support' );


add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
	return array(
		'width'  => 100,
		'height' => 100,
		'crop'   => 0,
	);
} );
