<?php
/**
 * Template Name: Homepage
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

	<section id="slider">
		<div id="sliderInner">
			<?php add_revslider('slider-2'); ?>
		</div>
	</section>
	<section class="transbg">
		<div id="hpContent" class="">
			<div class="container">
				<img src="<?php echo get_template_directory_uri(); ?>/images/kick-it-fwd-txt-logo.png">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		            the_content();
		        endwhile; endif; ?>
			</div>	
		</div>
		<div id="featuredSpots" class=" ">
			<div id="yellowTop" class="cf">
				<div class="container">
					<h3>Featured Items</h3> <a href="/shop/">shop all</a>
				</div>
			</div>
			<div id="featuredSliderContainer">
				<div class="container">
					<div id="featuredSpotSlider" class="">
						<?php 

							// The tax query
							$tax_query[] = array(
							    'taxonomy' => 'product_visibility',
							    'field'    => 'name',
							    'terms'    => 'featured',
							    'operator' => 'IN', // or 'NOT IN' to exclude feature products
							);

							// The query
							$the_query  = new WP_Query( array(
							    'post_type'           => 'product',
							    'post_status'         => 'publish',
							    'ignore_sticky_posts' => 1,
							    'posts_per_page'      => $products,
							    'orderby'             => $orderby,
							    'order'               => $order == 'asc' ? 'asc' : 'desc',
							    'tax_query'           => $tax_query // <===
							) );

							if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
						?>
							<div class="item">
								<a href="<?php the_permalink(); ?>"></a>
								<div class="picWrap">
									<div class="picInner">
										<img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'shop_single' ); echo $image[0]; ?>" />
									</div>
								</div>
								<div class="buttonWrap">
									<div class="productName"><?php the_title(); ?></div>
									<div class="productPrice"><?php echo woocommerce_template_loop_price(); ?></div>
									<div class="bottom"></div>
									<div class="bottomHover">View Product</div>
								</div>
							</div>
						<?php
								endwhile;
							endif;
							wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
		</div>
		<div id="howsItWork">
			<div class="container">
				<h3>How's It Work?</h3>
				<div id="hiwItems">
					<div class="item wow fadeInLeft delay0-2s">
						<?php
						$step = get_field('step_item_1');
						if( $step ): ?>
							<h4><?php echo $step['grey_text']; ?><span><?php echo  $step['main_title']; ?></span></h4>
							<?php echo $step['main_text']; ?>
						<?php endif; ?>
					</div>
					<div class="item wow fadeInLeft delay0-4s">
						<?php
						$step = get_field('step_item_2');
						if( $step ): ?>
							<h4><?php echo $step['grey_text']; ?><span><?php echo  $step['main_title']; ?></span></h4>
							<?php echo $step['main_text']; ?>
						<?php endif; ?>
					</div>
					<div class="item wow fadeInLeft delay0-6s">
						<?php
						$step = get_field('step_item_3');
						if( $step ): ?>
							<h4><?php echo $step['grey_text']; ?><span><?php echo  $step['main_title']; ?></span></h4>
							<?php echo $step['main_text']; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="<?php echo get_template_directory_uri(); ?>/scripts/slick/slick.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/scripts/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/scripts/slick/slick-theme.css">
	<script>
		jQuery(document).ready(function($){
			$("#featuredSpotSlider").slick({
		        dots: false,
		        infinite: true,
		        slidesToShow: 4,
		        slidesToScroll: 1,
		        variableWidth: true,
	          	responsive: [
			    	{
			      		breakpoint: 1198,
			      		settings: {
			        		slidesToShow: 3,
			     		}				     	
			    	},
			    	{
			      		breakpoint: 921,
			      		settings: {
			        		slidesToShow: 2,
			     		}
			     	},
			    	{
			      		breakpoint: 640,
			      		settings: {
			        		slidesToShow: 1,
			     		}
			     	}
				]
		    });
		});
	</script>
<?php get_footer(); ?>
