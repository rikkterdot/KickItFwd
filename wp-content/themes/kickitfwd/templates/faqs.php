<?php
/**
 * Template Name: FAQs
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Kick It Fwd!
 * @since Kick It Fwd!
 */

get_header();
?>
    <?php get_template_part( 'template-parts/page', 'title' ); ?>
    <?php get_template_part( 'template-parts/page', 'open' ); ?>

    <div id="faqContent" class="container contentWrapper">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
            the_content();
        endwhile; endif; ?>
    </div>
    <div id="faqsWrap" class="">
        <div class="container">
            <?php while( have_rows('faqs') ): the_row(); ?>
                <div class="faqItem">
                    <div class="question"><?php the_sub_field('question'); ?></div>
                    <div class="answer"><?php the_sub_field('answer'); ?></div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php get_template_part( 'template-parts/page', 'close' ); ?>
<?php get_footer(); ?>
