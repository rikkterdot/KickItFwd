<?php
/**
 * Template Name: Default
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Kick It Fwd!
 * @since Kick It Fwd!
 */

get_header();
?>
    <?php get_template_part( 'template-parts/page', 'title' ); ?>
    <?php get_template_part( 'template-parts/page', 'open' ); ?>

    <div id="" class="container contentWrapper">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
            the_content();
        endwhile; endif; ?>
    </div>
    <?php get_template_part( 'template-parts/page', 'close' ); ?>
<?php get_footer(); ?>
