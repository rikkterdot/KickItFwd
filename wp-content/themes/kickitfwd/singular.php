<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<link href="<?php bloginfo('template_directory'); ?>/css/blog.css?ver=<?php echo time(); ?>" rel="stylesheet" />

    
    <?php get_template_part( 'template-parts/page', 'title' ); ?>

<section>
	<div class="container">

		
       	<div id="blogSingleLeft">
	    	<?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if( wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-view-top' ) ){ ?>
                        <img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-view-top' ); echo $image[0]; ?>" />
                    <?php } else { ?>
                        <?php echo wp_get_attachment_image( '176', 'blog-view-top'); ?>
                    <?php }; ?>
                
                    <div class="entry-header">
                        <?php
                            if ( is_single() ) :
                                the_title( '<h1 class="entry-title">', '</h1>' );
                            else :
                                the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
                            endif;
                        ?>
                
                        <div class="entry-meta">
                            Posted on: <strong><?php the_time('F j, Y'); ?></strong> by: <strong><?php echo the_author_link(); ?></strong> <!-- | -->
                            <?php
                                //if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
                            ?>
                            <!--<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span>-->
                            <?php
                                //endif;
                
                                edit_post_link( __( '| Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
                            ?>
                        </div><!-- .entry-meta -->
                    </div><!-- .entry-header -->
                
                    <?php if ( is_search() ) : ?>
                    <div class="entry-summary">
                        <?php the_excerpt(); ?>
                    </div><!-- .entry-summary -->
                    <?php else : ?>
                    <div class="entry-content">
                        <?php
                            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ) );
                            wp_link_pages( array(
                                'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
                                'after'       => '</div>',
                                'link_before' => '<span>',
                                'link_after'  => '</span>',
                            ) );
                        ?>
                    </div><!-- .entry-content -->
                    <?php endif; ?>
                    
                    <?php
                        $posttags = get_the_tags();
                        if ($posttags) {
                            echo '<div id="tagsWrap">';
                            echo 'Tags: ';
                            foreach($posttags as $tag) {
                                echo $tag->name . ' '; 
                            }
                            echo '</div>';
                        }
                    ?>
                </article><!-- #post-## -->
            <?php endwhile; ?>
        </div>
        <div id="blogSingleRight">
            <?php get_sidebar(); ?>
        </div>
        <div style="clear:both;"></div>
    </div>
</section>

<?php get_footer(); ?>
