<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<!--Popup Search-->
			<div id="popupSearch">
				<div id="searchWrap" class="cf">
					<div id="closeSearch"><i class="fa fa-times" aria-hidden="true"></i></div>
					<input type="text" name="" id="" placeholder="Search...">
					<div id="searchBtn2"><i class="fa fa-search" aria-hidden="true"></i></div>
				</div>
			</div>
		<!--/Popup Search-->

		<header id="mainHdr">
			<div class="container">
				<div class="section group">
					<div id="navLeft" class="col span_1_of_4 no-top-bottom-margin">
						<a href="/" id="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/kick-it-fwd.png"></a>
					</div>
					<div id="mainNav" class="col span_3_of_4 no-top-bottom-margin">
						<div id="rightNav" class="right">
							<a href="#" id="searchBtn"><i class="fa fa-search"></i></a>
							<a href="/cart/"><i class="fa fa-shopping-cart"></i></a>
						</div>
						<div id="navWrap" class="right">
							<?php wp_nav_menu( array('menu' => 'Navigation' )); ?>
						</div>
					</div>
				</div>
			</div>
			<div id="headerAngle">
				<div class="container">
					<a href="//twitch.tv/kickitfwd" target="_blank" class="live twitch"></a>
				</div>
			</div>
		</header>