<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>		
		<div id="instagramWrap"></div>
		<footer>
			<div class="container">
				<div id="linksWrap" class="left">
					<div class="linkColumn">
						<strong>Shop</strong>
						<?php wp_nav_menu( array('menu' => 'Footer - Services' )); ?>
					</div>
					<div class="linkColumn col1">
						<strong>Company</strong>
						<?php wp_nav_menu( array('menu' => 'Footer - Company' )); ?>						
					</div>
				</div>
				<div id="logoSocials" class="right">
					<div id="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logo.png"></div>
					<div id="socialIcons">
						<a href="<?php the_field('twitch_url','options'); ?>" target="_blank"><i class="fab fa-twitch"></i></a>
						<a href="<?php the_field('discord_url','options'); ?>" target="_blank"><i class="fab fa-discord"></i></a>
						<?php while( have_rows('social_icons', 'options') ): the_row(); ?>
							<a href="<?php the_sub_field('social_url'); ?>" target="_blank" alt="<?php the_sub_field('social_name'); ?>" title="<?php the_sub_field('social_name'); ?>"><?php the_sub_field('social_icon'); ?></i></a>
		                <?php endwhile; ?>
					</div>
				</div>
				<div style="clear: both;"></div>
				<hr>
				<div id="footerLeftText" class="left">
					<?php the_field('footer_fine_print','options'); ?>
				</div>
				<div id="footerRight" class="right">
				</div>
				<div style="clear: both;"></div>
			</div>
		</footer>
		<?php wp_footer(); ?>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/reveal.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/scripts.js"></script>
		
		<script src="<?php echo get_template_directory_uri(); ?>/scripts/InstagramFeed.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/scripts/marquee.js"></script>
		<!-- <script src="<?php echo get_template_directory_uri(); ?>/scripts/ticker/jquery.mousewheel.min.js" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/scripts/ticker/jquery.smoothdivscroll-1.2-min.js" type="text/javascript"></script> -->

		<script>
			jQuery(document).ready(function(){

			        new InstagramFeed({
			            'username': 'kickitfwd',
			            'container': document.getElementById("instagramWrap"),
			            'display_profile': false,
			            'display_biography': false,
			            'display_gallery': true,
			            'display_captions': false,
			            'callback': function(){
			            	/*jQuery(".instagram_gallery").smoothDivScroll({ 
								mousewheelScrolling: false,
								manualContinuousScrolling: true,
								visibleHotSpotBackgrounds: "always",
								autoScrollingMode: "always",
								autoScrollingInterval: "30"
							});
							jQuery(".instagram_gallery").hover(function(){
								jQuery(".instagram_gallery").smoothDivScroll("stopAutoScrolling");
							}, function(){
								console.log("hoveroff");
								jQuery(".instagram_gallery").smoothDivScroll("startAutoScrolling");
							});	
							jQuery(".instagram_gallery").smoothDivScroll("startAutoScrolling");*/
							jQuery('.instagram_gallery').liMarquee({
								direction: 'left',	
								loop:-1,			
								scrolldelay: 0,		
								scrollamount:80,	
								circular: true,		
								drag: true			
							});
			            },
			            'styling': true,
			            'items': 12,
			            'items_per_row': 12,
			            'margin': 1,
			            'lazy_load': false,
			            'on_error': console.error
			        });
			        
			});
		</script>

	</body>
</html>
