<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php get_template_part( 'template-parts/page', 'title' ); ?>
	<?php get_template_part( 'template-parts/page', 'open' ); ?>

	<!--Product-->
		<div id="product-wrap">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php wc_get_template_part( 'content', 'single-product' ); ?>
			<?php endwhile; // end of the loop. ?>
		</div>
	<!--/Product-->
	<script>
		jQuery(document).ready(function($){
			jQuery("#product-left-wrap #thumbnails-wrap a").click(function(){
				var fullImg = jQuery(this).attr("href");
				var title = jQuery(this).find("img").attr("alt");
				var imgExtension = "." + fullImg.substr(fullImg.length - 3);
				var imageSliced = fullImg.slice(0,-4);
				var displayImage = fullImg;
				jQuery("#product-left-wrap #main-image a").attr("src", fullImg).find("img").attr("src", displayImage).attr("srcset", "");
				return false;
			});

			jQuery("#product-left-wrap #main-image a").click(function(){
			});
		});
	</script>
	<?php get_template_part( 'template-parts/page', 'close' ); ?>

<?php get_footer( 'shop' ); ?>
