<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

global $post;
$terms = wp_get_post_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) $categories[] = $term->slug;

$isLiveBreak = false;
if ( in_array( 'live-breaks', $categories ) ):
	$isLiveBreak = true;
endif;

?>
<div class="container w">
<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
</div>
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container w">
		<div id="product-left-wrap" class="left cf">
			<?php
				/**
				 * woocommerce_before_single_product_summary hook.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
				//echo woocommerce_show_product_images();
			?>
		</div>
		<?php if( $isLiveBreak ): ?>
			<div id="product-right-wrap" class="right liveBreak">
		<?php else: ?>
			<div id="product-right-wrap" class="right notLiveBreak">
		<?php endif; ?>
			 <?php echo woocommerce_template_single_title(); ?>
			 <div id="price-wrap">
			 	<?php echo woocommerce_template_single_price(); ?>
			 </div>
			 <hr>
			 <?php
			 	echo woocommerce_template_single_add_to_cart();
			 ?>
			 <hr>
			 <div id="prod-desc">
			 	<?php the_content(); ?>
			 </div>
			 </div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<div class="container">
		<div class="reldivider"></div>
	</div>
	<script>
		jQuery(".woocommerce .variations .swatch-control.radio-select ul li").click(function(){
			if( jQuery(this).hasClass("active") ){
				//do nothing
			} else {
				jQuery(this).parent().find("li.active").removeClass("active");
				jQuery(this).addClass("active");
			};
		});
	</script>

	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
		echo woocommerce_output_related_products();
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
