<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<?php 

global $wp_query;
$cat = $wp_query->query_vars['product_cat'];
$catobject = get_term_by( 'slug', $cat, 'product_cat' );

$args = array(
	'taxonomy' => 'product_cat',
	'parent' => 0,
	'hide_empty' => 0,
	'orderby' => 'name',
);
$categories = get_categories($args);
$cat_counter = 0;

 ?>

    <div id="categoryListWrapper">
    	<div id="mobile-category-btn">Categories <i class="fa fa-angle-down" aria-hidden="true"></i></div>
	    <div id="mobile-categories">
	    	<ul>
	    		<li<?php echo $catobject->term_id == $category->term_id || $catobject->parent == $category->term_id ? ' class="	"' : '' ?>>
	    					<a class="catLink" href="/shop/">Shop All</a>
	    	    </li>
	    		<?php foreach($categories as $category):  ?>
	    			<?php if( $category->name == "Uncategorized" ): ?>
	    			<?php else: ?>
		    	    	<?php $subcategories = get_categories( array('taxonomy' => 'product_cat','parent' => $category->term_id,'hide_empty' => 1,'orderby' => 'name',) ); ?>
		    	        
		    	        <li<?php echo $catobject->term_id == $category->term_id || $catobject->parent == $category->term_id ? ' class="active"' : '' ?>>
		    	            <a class="catLink" href="<?=get_term_link( $category );?>"><?php echo $category->name;?><?php if( $subcategories ): ?> <i class="fa fa-angle-down" aria-hidden="true"></i><?php endif; ?></a>
		    	            <?php
		    							$args = array( 'post_type' => 'product', 'posts_per_page' => 5, 'product_cat' => $category->slug );
		    							$products = new WP_Query( $args );
		    							
		    							if( $products->have_posts() ){
		    						?>
		    	                    	<?php if( $subcategories ): ?>
		    	                			<ul class="prod-sub">
		    	                        
		    			                        <li<?php echo $catobject->term_id == $subcategory->term_id || $catobject->parent == $subcategory->term_id ? ' class="active"' : '' ?>>
		    	                        	<a class="catLink-sub" href="<?=get_term_link( $category );?>">Shop All <?php echo $category->name; ?></a>
		    	                        </li>
		    	
		    	                    	<?php foreach( $subcategories as $subcategory ): ?>
		    	                        	<?php 
		    											$subcatprodargs = array( 'post_type' => 'product', 'posts_per_page' => 5, 'product_cat' => $subcategory->slug );
		    											$subproducts = new WP_Query($subcatprodargs);
		    										?>
		    	                        	<li<?php echo $catobject->term_id == $subcategory->term_id || $catobject->parent == $subcategory->term_id ? ' class="active"' : '' ?>>
		    	                            	<a class="catLink-sub" href="<?php echo get_term_link( $subcategory );?>"><?php echo $subcategory->name?></a>
		    	                            </li>
		    	                        <?php endforeach; ?>
		    	                    
		    	                    		</ul>
		    	                        <?php endif; ?>
		    	            <?php }; ?>
		    	        </li>
	    			<?php endif; ?>
	    	    <?php endforeach; ?>
	    	</ul>
	    </div>
    </div>                

    <script>
    	jQuery(document).ready(function($){
    		//when they click on catlink
    		//check if parent has class active.
    		//if it does, close menu
    		//if it doesnt
    		//check to see if it has subcategories.1
    		//if it does, open and give parent actives
    		//if not, go to link

    		$("#mobile-category-btn").click(function(){
    			if( $(this).hasClass('active') ){
    				$(this).removeClass("active").next().slideUp();
    			} else {
    				$(this).addClass("active").next().slideDown();
    			}
    		});
    		$(".catLink").click(function(){
    			var $parent = $(this).closest("li");
    			var $sublinks = $parent.find(".prod-sub");

    			if( $sublinks.length ){
	    			if( $parent.hasClass("active") ){
	    				$sublinks.slideUp();
	    				$parent.removeClass("active");
	    			} else {
	    				$parent.addClass("active");
	    				$sublinks.slideDown();
	    			};
	    			return false;
    			} else {

    			};
    		});

    		$("#categoryListWrapper ul li.active").find(".prod-sub").slideDown();
    	});
    </script>


<?php wp_reset_query(); 

