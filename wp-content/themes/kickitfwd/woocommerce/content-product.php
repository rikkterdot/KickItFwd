<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;




// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}


?>
<li <?php post_class( $classes ); ?>>
	<a href="<?php the_permalink(); ?>"></a>
	<div class="img-wrap">
		<div class="imgInner">
			<?php
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'shop_catalog' );
				if( $image ):
					$image = $image[0];
				endif;
			?>
			<img class="featuredPic" src="<?php echo $image; ?>">
		</div>
	</div>
	<div class="divider"><div class="divider-rollover"></div></div>
	<div class="prod-title-wrap"><?php echo woocommerce_template_loop_product_title(); ?></div>
	<div class="product-price"><?php echo woocommerce_template_loop_price(); ?></div>
</li>
