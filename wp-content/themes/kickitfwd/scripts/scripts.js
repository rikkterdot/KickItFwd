jQuery(document).ready(function($){

	jQuery(window).load(function(){
		checkTop();
	});
	jQuery(window).scroll(function(){
		checkTop();
	});

	function checkTop(){
		if(!jQuery(window).scrollTop()) { //abuse 0 == false :)
		  jQuery("#mainHdr").removeClass("notAtTop");
		} else {
		  jQuery("#mainHdr").addClass("notAtTop");
		};

	};

	$("header#mainHdr #searchBtn").click(function(){
		$("#popupSearch").css({top: "0px"});
		$("#searchWrap input").focus();
		return false;
	});

	$("#popupSearch #closeSearch").click(function(){
		$("#popupSearch").css({top: "-200px"});
	});

	$("#searchWrap input").keypress(function(e){
		var searchTerm = $(this).parent().find("input").val();

		if( e.which == 13 ){
			doSearch(searchTerm);
		};
	});

	$("#searchWrap #searchBtn2").click(function(){
		var searchTerm = $(this).parent().find("input").val();
		doSearch(searchTerm);
	});


	function doSearch(term){
		window.location = "/?s=" + term + "&post_type=product";
	};


	new WOW().init();

});