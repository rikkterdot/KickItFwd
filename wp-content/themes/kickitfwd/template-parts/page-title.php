<div id="pageTitle">
	<div class="container">
		<?php if( is_woocommerce() ): ?>
			<h1>Shop</h1>
		<?php else: ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
	</div>
</div>
